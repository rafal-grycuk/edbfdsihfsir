import torch
import torchvision
import os
import cv2
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import sys
from scipy.ndimage.measurements import label
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from torchvision.datasets import MNIST
from multiprocessing import Pool, freeze_support
from slackIntegration import logSlack
from torchsummary import summary

# %% autoencoder
class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=4, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2), 
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),           
            nn.Conv2d(in_channels=32, out_channels=1, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.decoder = nn.Sequential(    
             nn.ConvTranspose2d(in_channels=1, out_channels=32, kernel_size=3, stride=2, padding=1),
            nn.ZeroPad2d((0, 1, 0, 1)),
            nn.ReLU(inplace=True),        
            nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=3, stride=2, padding=1),
            nn.ZeroPad2d((0, 1, 0, 1)),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=3, stride=2, padding=1),
            nn.ZeroPad2d((0, 1, 0, 1)),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(in_channels=8, out_channels=4, kernel_size=3, stride=2, padding=1),
            nn.ZeroPad2d((0, 1, 0, 1)),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(in_channels=4, out_channels=1,  kernel_size=3, stride=2, padding=1),
            nn.ZeroPad2d((0, 1, 0, 1)),
            nn.ReLU(inplace=True),        
            # nn.ConvTranspose2d(in_channels=4, out_channels=1,  kernel_size=3, stride=2, padding=1),
            nn.Tanh()
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

    def encoded(self, x):
        x = self.encoder(x)
        return x

    def to_img(self, x):
        x = 0.5 * (x + 1)
        x = x.clamp(0, 1)
        x = x.view(x.size(0), 1, 512, 512)
        return x

#%% helper functions
def save_encoded_hash(descriptor, image_file_path, destination_image_directory):
    np_desc= descriptor.cpu().detach().numpy()
    file_path = "{dst_dic}/{base_file_name}_DESC.txt".format(dst_dic=destination_image_directory, base_file_name=os.path.basename(image_file_path))
    np.savetxt(file_path, np_desc, delimiter=',')
    print("Desc Saved {file_path}".format(file_path=file_path))
    # with open(file_path, "w") as file:
    #     # desc_str = ','.join(str(x) for x in np_desc)
    #     file.write(np_desc)

if __name__ == '__main__':
    try:
        is_cuda_available = torch.cuda.is_available()
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        print("Device: {val}".format(val=device))
        print('__Python VERSION:', sys.version)
        print('__pyTorch VERSION:', torch.__version__)
        print('__CUDA VERSION')

        print('__CUDNN VERSION:', torch.backends.cudnn.version())
        print('__Number CUDA Devices:', torch.cuda.device_count())
        print('__Devices')
        print('Active CUDA Device: GPU', torch.cuda.current_device())
        print('Available devices ', torch.cuda.device_count())
        print('Current cuda device ', torch.cuda.current_device())
        start = time.time()
# %% run autoencoder learning

        if not os.path.exists('./dc_img_edge'):
            os.mkdir('./dc_img_edge')
        num_epochs = 20
        batch_size = 30
        learning_rate = 1e-3

        img_transform = transforms.Compose([transforms.transforms.Grayscale(num_output_channels=1),
                                    transforms.transforms.ToTensor()])

        datapath = "./AIA_Images/SolarData_TG/"
        dataset = torchvision.datasets.ImageFolder(datapath, transform=img_transform)
        # dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False)
        dataloader = DataLoader(dataset,shuffle=False)
        torch.cuda.empty_cache()
        model = autoencoder().cuda()
        summary(model, (1, 512,512))
        criterion = nn.MSELoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=1e-5)

        for epoch in range(num_epochs):
            torch.cuda.empty_cache()
            for i, data in zip(range(0, len(dataloader.dataset.samples)), dataloader):
                torch.cuda.empty_cache()
                img, _ = data
                # print(img)
                img = Variable(img).cuda()

                # ===================forward=====================
                output = model(img)
                loss = criterion(output, img)
                # ===================backward====================
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                if epoch == num_epochs-1:
                    file_path =os.path.splitext(os.path.basename(dataloader.dataset.samples[i][0]))[0]
                    reconstructed_img = model.to_img(output.cpu().data)
                    save_image(reconstructed_img, "./AIA_Images/SolarData_TG/Reconstructed/{rec_img_path}_REC.jpg".format(rec_img_path=file_path))
                    print("./AIA_Images/SolarData_TG/Reconstructed/{rec_img_path}_REC.jpg".format(rec_img_path=file_path))
            # ===================log========================
            print('epoch [{}/{}], loss:{:.4f}'
                  .format(epoch+1, num_epochs, loss.data.item()))
            if epoch % 2 == 0:
                pic = model.to_img(output.cpu().data)
                save_image(pic, './dc_img_edge/image_{}.png'.format(epoch))
        torch.save(model.state_dict(), './conv_autoencoder_with_edged_images.pth')

#%% Get encoded hashes        
        encodedModel = model.encoded
        torch.cuda.empty_cache()
        for i, data in zip(range(0, len(dataloader.dataset.samples)), dataloader):
            torch.cuda.empty_cache()
            img, _ = data
                # print(img)
            img = Variable(img).cuda()
            output_hash = encodedModel(img)
            output_hash = output_hash.view(1, 16, 16)
            #print(output)
            file_path = dataloader.dataset.samples[i][0]
            save_encoded_hash(output_hash.flatten(), file_path, "./AIA_Images/SolarData_TG/Desc")
    except Exception as e:
        # logSlack(str(e))
        raise
